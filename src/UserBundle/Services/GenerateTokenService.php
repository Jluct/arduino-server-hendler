<?php

namespace UserBundle\Services;

/**
 * Class GenerateTokenService
 * @package UserBundle\Services
 */
class GenerateTokenService
{
    /**
     * @param array $data
     * @return string
     */
    public function generateToken(array $data)
    {
        $string = '';
        foreach ($data as $item) {
            $string = sprintf('%s%s', $string, $item);
        }

        return md5($string);
    }
}