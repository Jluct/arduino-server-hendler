<?php

namespace UserBundle\Services;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use UserBundle\Entity\User;

/**
 * Class LogoutApiService
 * @package UserBundle\Services
 */
class LogoutApiService implements LogoutSuccessHandlerInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * LogoutApiService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository('UserBundle:User')
            ->findOneBy(['apiKey' => $request->headers->get('authorization')]);
        $user->setApiKey('');
        $this->entityManager->persist($user);
        $response = new JsonResponse();
        try {
            $this->entityManager->flush($user);
            $response->setData(['success' => true]);
        } catch (\Exception $e) {
            $response->setStatusCode(500);
            $response->setData(['success' => false, 'errors' => [$e->getMessage()]]);
        }

        return $response;
    }
}