<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class DefaultController
 * @package UserBundle\Controller
 */
class DefaultController extends Controller
{
    const AUTH_LOGIN_ACTION = 'login';
    const VALIDATION_TOKEN_ACTION = 'valid-token';

    /**
     * @Route("/login", name=DefaultController::AUTH_LOGIN_ACTION)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $arrayResponse = [
            'success' => true
        ];

        /**
         * @var User $user
         */
        $user = $this->getUser();
        $user->setApiKey(
            $this->get('generate.token')->generateToken(
                [
                    $user->getUsername(),
                    $user->getSalt(),
                    (new \DateTime())->format('Y-m-d H:i:s')
                ]
            )
        );
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        try {
            $em->flush();
        } catch (\Exception $e) {
            $this->get('logger')->addError($e->getMessage());
            $arrayResponse['success'] = false;
        }

        if ($arrayResponse['success']) {
            $arrayResponse['user']['username'] = $user->getUsername();
            $arrayResponse['user']['token'] = $user->getApiKey();
        }

        return $this->json($arrayResponse);
    }

    /**
     * @Route("/validation-token.{_format}", name=DefaultController::VALIDATION_TOKEN_ACTION,
     *     requirements={"_format"="json"}, defaults={"_format": "json"})
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    public function validTokenAction(Request $request)
    {
        $arrayResponse = [
            'success' => true
        ];

        return $this->json($arrayResponse);
    }
}
