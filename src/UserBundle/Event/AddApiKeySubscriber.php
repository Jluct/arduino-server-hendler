<?php

namespace UserBundle\Event;


use AppBundle\Services\AppendDataJSON;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use UserBundle\Services\GenerateTokenService;
use Doctrine\ORM\EntityManagerInterface;
use UserBundle\Entity\User;

/**
 * Class AddApiKey
 * @package UserBundle\Event
 */
class AddApiKeySubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var GenerateTokenService
     */
    protected $generator;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var AppendDataJSON
     */
    protected $appendDataJSON;

    /**
     * AddApiKeySubscriber constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationChecker $authorizationChecker,
        EntityManagerInterface $entityManager,
        GenerateTokenService $generator,
        AppendDataJSON $appendDataJSON
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->entityManager = $entityManager;
        $this->generator = $generator;
        $this->appendDataJSON = $appendDataJSON;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::RESPONSE => 'injectApiKey'];
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function injectApiKey(FilterResponseEvent $event)
    {
        $authToken = $this->tokenStorage->getToken();
        if (!empty($authToken) && !$authToken instanceof AnonymousToken) {
            $user = $authToken->getUser();
            if ($user && $user instanceof User && $this->authorizationChecker->isGranted('ROLE_USER') && $user->getApiKey()) {
                if (!$this->updateApiKey()) {
                    $event->getResponse()->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                    $event->getResponse()->setContent(
                        $this->appendDataJSON->addData(
                            $event->getResponse()->getContent(),
                            ['Not update apiKey. Reload page'],
                            'errors'
                        )
                    );
                }
                $event->getResponse()->headers->set('X-AUTH-TOKEN', $user->getApiKey());
            }
        }
    }

    /**
     * @return bool
     */
    private function updateApiKey()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $user->setApiKey(
            $this->generator->generateToken(
                [
                    $user->getUsername(),
                    $user->getSalt(),
                    (new \DateTime())->format('Y-m-d H:i:s')
                ]
            )
        );
        $this->entityManager->persist($user);
        try {
            $this->entityManager->flush();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}