<?php

namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class JbUserCreateAdminCommand
 * @package UserBundle\Command
 */
class JbUserCreateAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('jb:user:create:admin')
            ->setDescription('Creates a user with administrator role')
            ->addOption('login', 'l', InputOption::VALUE_REQUIRED, 'login')
            ->addOption('fullname', 'f', InputOption::VALUE_REQUIRED, 'Full name')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'Password')
            ->addOption('email', 'm', InputOption::VALUE_REQUIRED, 'Email')
            ->addOption('active', 'a', InputOption::VALUE_OPTIONAL, 'Active user', true);
    }

    /**
     * Created admin user
     * Example:
     * php bin/console ...
     * jb:user:create:admin -l admin -f administrator -p 123456 -m admin1@mail.ru -a true
     * OR
     * jb:user:create:admin --login=admin --fullname=admin --password=123456 --email=admin@mail.ru --active=true
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = $this->getContainer()->get('user.create');
        $data = [];

        $data['login'] = $input->getOption('login');
        $data['fullname'] = $input->getOption('fullname');
        $data['password'] = $input->getOption('password');
        $data['email'] = $input->getOption('email');
        $data['active'] = $input->getOption('active') ? 1 : 0;
        $data['role'] = ['ROLE_ADMIN'];
        $service->createUser($data);

        $output->writeln('<info>User created</info>');
        return 0;
    }
}
