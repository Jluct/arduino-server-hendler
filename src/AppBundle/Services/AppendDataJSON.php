<?php

namespace AppBundle\Services;

/**
 * Class AppendDataJSON
 * @package AppBundle\Services
 */
class AppendDataJSON
{
    /**
     * @param string $json_str
     * @param array $dataArray
     * @param string $key
     * @return null|string
     */
    public function addData(string $json_str, array $dataArray, string $key)
    {
        if (!$this->isJSON($json_str)) {
            return null;
        }
        $data = json_decode($json_str);
        $data[$key] = $dataArray;

        return json_encode($data);
    }

    /**
     * @param string $data
     * @return bool
     */
    private function isJSON(string $data)
    {
        if (!empty($data)) {
            $tmp = json_decode($data);
            return (
                json_last_error() === JSON_ERROR_NONE
                && (is_object($tmp) || is_array($tmp))
            );
        }

        return false;
    }
}