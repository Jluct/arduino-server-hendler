<?php

namespace AppBundle\Services;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class SerializerService
 * @package AppBundle\Services
 */
class SerializerService
{
    const FORMAT_JSON = 'json';

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SerializerService constructor.
     */
    public function __construct()
    {
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = [$normalizer];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @param $data
     * @param string $format
     * @param array $groups
     * @return string
     */
    public function encode($data, string $format, array $groups){
        $content = $this->serializer->normalize($data, $format, $groups);

        return $content;
    }
}