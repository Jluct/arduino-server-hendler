<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    const HOMEPAGE_ACTION = 'homepage';
    const SETTINGS_ACTION = 'settings';
    const LOCATIONS_ACTION = 'locations';
    const LOCATION_SENSORS_ACTION = 'location_sensors';
    const DEVICES_ACTION = 'devices';

    /**
     * @Route("/", name=DefaultController::HOMEPAGE_ACTION,
     *     requirements={"_format"="json"}, defaults={"_format": "json"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function defaultAction(Request $request)
    {
        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/settings.{_format}", name=DefaultController::SETTINGS_ACTION,
     *     requirements={"_format"="json"}, defaults={"_format": "json"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function settingsAction(Request $request)
    {
        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/locations.{_format}", name=DefaultController::LOCATIONS_ACTION,
     *     requirements={"_format"="json"}, defaults={"_format": "json"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function locationsAction(Request $request)
    {
        $locations = $this->getDoctrine()->getRepository('AppBundle:Locations')
            ->findBy([
                'user' => $this->getUser()->getId(),
                'active' => 1
            ]);
        $serializer = $this->get('serializer_service');
        $locations = $serializer->encode($locations, $serializer::FORMAT_JSON, ['attributes' => ['id', 'name']]);

        return new JsonResponse(['success' => true, 'locations' => $locations]);
    }

    /**
     * @Route("/location/{id}.{_format}", name=DefaultController::LOCATION_SENSORS_ACTION,
     *     requirements={"_format"="json", "id"="\d+"}, defaults={"_format": "json"})
     *
     * @param int $id
     * @return JsonResponse
     */
    public function locationSensorsAction($id)
    {
        $location = $this->getDoctrine()->getRepository('AppBundle:Locations')
            ->findOneBy([
                'user' => $this->getUser()->getId(),
                'id' => $id
            ]);
        if (empty($location)) {
            throw new NotFoundHttpException();
        }
        $sensors = $this->getDoctrine()->getRepository('AppBundle:Sensor')
            ->findBy([
                'location' => $id,
                'active' => 1
            ]);
        $serializer = $this->get('serializer_service');
        $sensors = $serializer->encode($sensors, $serializer::FORMAT_JSON, ['attributes' => ['id', 'name', 'updateAt', 'data']]);

        return new JsonResponse(['success' => true, 'sensors' => $sensors]);
    }

    /**
     * @Route("/devices.{_format}", name=DefaultController::DEVICES_ACTION,
     *     requirements={"_format"="json"}, defaults={"_format": "json"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function devicesAction(Request $request)
    {
        return new JsonResponse(['success' => true, 'devices' => []]);
    }
}
